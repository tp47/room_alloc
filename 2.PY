import sys
import collections

def hall():
    """
    Print and exit!
    """
    print("hallelujah")
    exit(0)


def find_person(name, rooms):
    """
    Return the index number of a room for a name
    """
    for i,r in enumerate(rooms):
        if name in r:
            return i


def get_like_dislikes(room, likes, dislikes):
    """
    For a room,
    get the set of all their likes
    get the set of all their dislikes
    """
    room_dislikes = []
    room_likes = []
    for person in room:
        room_likes.append(person)
        for l in likes[person]:
            room_likes.append(l)
        for d in dislikes[person]:
            room_dislikes.append(d)
            if d in room_likes:
                hall()
    return (
        list(set(room_likes)),
        list(set(room_dislikes))
    )

def intersects(lst1, lst2):
    """
    Do the lists have intersecting elements?
    Return true if yes.
    """
    if [value for value in lst1 if value in lst2] != []:
        return True
    return False


def breakup_tests(curr, l_curr, rooms, likes, dislikes):
    if not l_curr:
        hall()
    (curr_likes, our_dislikes
    ) = get_like_dislikes(
        rooms[curr],
        likes,
        dislikes)

    (   l_likes,
        l_dislikes
    ) = get_like_dislikes(
        rooms[l_curr],
        likes,
        dislikes)

    # print("our_likes")
    # print(our_likes)

    # print(our_dislikes)

    # print("l_likes")
    # print(l_likes)

    # print("l_dislikes")
    # print(l_dislikes)


    # Does my love's room's people, not like our likes?
    if intersects(l_dislikes, our_likes):
        return True

    if intersects(l_dislikes, rooms[curr]):
        return True

    # Other way around
    if intersects(l_likes, our_dislikes):
        return True

    if intersects(rooms[l_curr], our_dislikes):
        return True

    return False






# Main logic of the program starts here:
dislikes = collections.defaultdict(list)
likes = collections.defaultdict(list)
names = []

for line in sys.stdin:
    if line == "\n":
        # Blank line
        continue
    if line[0] == "-":
        # Dislike someone
        dislikes[our_name].append(line[2:-1])
        continue
    if line[0] == "+":
        # Prefer someone
        likes[our_name].append(line[2:-1])
        continue
    names.append(line.strip())
    our_name = names[-1]

rooms = [ [name] for name in names]

for name in names:
    for l in likes[name]:
        curr = find_person(name, rooms)
        l_curr = find_person(l, rooms)
        if curr == l_curr:
            # print("Passing on...")
            continue

        # conditions to break up...
        if breakup_tests(curr, l_curr,
                      rooms, likes, dislikes):
            hall()
        # combine rooms
        rooms[curr].extend(rooms[l_curr])
        del rooms[l_curr]

# Sort order within the rooms
sorted_rooms = []
for r in rooms:
    r = sorted(r)
    sorted_rooms.append(r)

# print("-"*10)
# Sort the room order

for r in sorted(sorted_rooms):
    print(" ".join(r).strip())
