# Address Labels

# In the past, Santa Claus could remember all the names and addresses of the children, but as he does not get any younger and the children become more and more, he cannot remember everyone. For this reason, he wants to attach an address label to every gift, so that he knows exactly which gift belongs to which person and household. Since the packages are also different in size, of course, also the labels are. Unfortunately, the addresses in some places around the world are not as short as they are in Germany, because sometimes every paving stone is used as a marker to identify a place of residence. This address should now be nicely placed in the middle of the sticker.
# Input

# The first line of the input is the maximum width N of the sticker without border with 10 <= N <= 100

# In the next line is the number of address lines M mit 5 <= M <= 1000

# Finally, the individual lines of the address are given, which are each written in a new line. Valid characters are limited to uppercase and lowercase letters, numbers from 0 to 9 and the space.
# Output

# The output consists only of the address label including a complete border with the "*" - sign. If an entered address line is too wide for the address label, a space is used as separator. Since you do not want to waste any space during separating, you startfrom the beginning and try to fit as many sub-words as possible into one line of the sticker. In addition, the contents of a line should be written in the middle of the line. If this is not possible due to the width of the sticker, the content may be moved one place to the left.

# input:
# 19
# 5
# Herr
# Max Mustermann
# Beispielsstraße 7
# 33333 Musterstadt
# Deutschland

# output:
# *********************
# *       Herr        *
# *  Max Mustermann   *
# * Beispielsstraße 7 *
# * 33333 Musterstadt *
# *    Deutschland    *
# *********************

def cprint(text, width):
    centered = text.center(width)
    l = len(text)
    oddWidth = width % 2 != 0
    oddL = l % 2 != 0
    if (oddWidth and not oddL) or (not oddWidth and oddL):
        if width-l > 3:
            # if width-l %2 != 0 and
            if centered[0]==" ":
                centered = centered[1:] + " "
    print(f"*{centered}*")


width = int(input())
lines = int(input())

address = []

for _ in range(lines):
    address.append(input())

line = "*" * (width + 2)
print(line)
for a in address:
    sub_width = len(a)
    sub = rest = a
    while width <= sub_width:
        spaces = [i for i,
                  x in enumerate(rest)
                  if
                  x == " "
                  and
                  i <= width]
        if spaces == []:
            raise Exception("Invalid word length")
        last = spaces[-1] # Last space, which still fits
        sub = rest[:(last)]
        cprint(sub, width)
        rest = rest[(last+1):]
        sub_width = len(rest)
        # print(sub_width)
    else:
        a = rest
    cprint(a, width)


print(line)
